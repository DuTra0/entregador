package com.marlongas.motoboy;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public AlertDialog.Builder dialog;
    public WebView webView;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        progressBar = findViewById(R.id.progress);
        webView = findViewById(R.id.web_view_delivery);

        progressBar.setVisibility(View.INVISIBLE);

        Browser myBrowser = new Browser();
        myBrowser.setActivity(this);
        myBrowser.setProgressBar(progressBar);
        webView.setWebViewClient(myBrowser);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebChromeClient(new WebChrom(this));
        webView.getSettings().setGeolocationDatabasePath(getApplicationContext().getFilesDir().getPath());

        dialog = new AlertDialog.Builder(this);
        requestForPermissionLocation(this);
        abrirPagina();
    }

    @Override
    public void onBackPressed() {
        if (webView.isFocused() && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public static void requestForPermissionLocation(final Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_DENIED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                promptForPermissionsDialog(activity, "Permissao necessaria", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
                    }
                });

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
            }

        }
    }
    private static void promptForPermissionsDialog(Context context, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton("OK", onClickListener)
                .setNegativeButton("CANCELAR", null)
                .create()
                .show();
    }

    private void abrirPagina() {

        try {
            WebSettings ws = webView.getSettings();

            if (true) {
                progressBar.setVisibility(View.INVISIBLE);
                String url = getString(R.string.url_site);
                ws.setAllowFileAccess(true);
                ws.setGeolocationEnabled(true);
                ws.setCacheMode(WebSettings.LOAD_DEFAULT);
                ws.setJavaScriptEnabled(true);
                ws.setAppCacheMaxSize(5 * 1024 * 1024); //5mb
                ws.setSupportZoom(false);
                ws.setAppCacheEnabled(true);
                ws.setLoadsImagesAutomatically(true);
                ws.setAppCacheEnabled(true);
                ws.setDatabaseEnabled(true);
                ws.setDomStorageEnabled(true);
                ws.setJavaScriptCanOpenWindowsAutomatically(true);
                webView.loadUrl(url);

            } else {
                progressBar.setVisibility(View.VISIBLE);
                ws.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                Toast.makeText(this, "No momento você está sem conexão com a internet.",
                        Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Ocorreu um erro inteno!",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();

        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        finish();
        return true;
    }

}