package com.marlongas.motoboy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;

public class WebChrom extends WebChromeClient {

    public AlertDialog.Builder dialog;
    private Context context;

    public WebChrom(Context context){
        this.context = context;
    }

    @Override
    public void onGeolocationPermissionsShowPrompt(final String origin,
                                                   final GeolocationPermissions.Callback callback) {
        dialog = new AlertDialog.Builder(context);

//dialog perguntando se o usuário permite ou não pegar sua localização
        dialog.setTitle("Acessar sua localização");
        dialog.setMessage("O aplicativo quer acessar sua localização. Você" +
                " permite?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Permitir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.invoke(origin, true, true);
                Log.i("localizacao", origin);
            }
        });
        dialog.setNegativeButton("Negar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.invoke(origin, false, true);
                Log.i("localizacao", origin);
            }
        });
        dialog.create();
        dialog.show();
    }
}
